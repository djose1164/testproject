FROM python:3-slim
RUN python -m pip install DateTime
RUN apt update && apt -y upgrade && apt-get -y install vim

COPY ./test/test.py /home
CMD ["python", "/home/test.py"]

